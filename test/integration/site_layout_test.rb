require 'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end
  
  test "layout links test" do
    
    get root_path
    assert_template 'static_pages/home'
    assert_select 'a[href=?]', about_path
    assert_select 'a[href=?]', contact_path
    
  
  end
  
  test "testing contact link" do
  
    get contact_path
    assert_template "static_pages/contact"
    assert_select "a[href=?]", contact_path, count:2
    
  end
  
 
  
  
end
