require 'test_helper'

class UserTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  
  def setup
  
    @user=User.new(name:"sameer", email:"sameercourse@gmail.com", password:"sameercourse", password_confirmation:"sameercourse")
    
    
  end
  
  
  test "testing if user is valid - without any validations in the USER model" do
    
   assert @user.valid?
    
  end
  
  
  test "name and email presence validation test" do
    
    @user.email=" "
    @user.name="  "
  
  assert_not @user.valid?
  
    
  end
  
  test "to check the maximum lenght vaalidation of name column in the schema" do
  
  @user.name="a"*51;
  
  assert_not @user.valid?
  
  end
  
  
  test "email regular expression validation" do
  
  @user.email="gube$gmail.com"
  
  assert_not @user.valid?
  
  end
  
  
  test "email uniqueness validation using the uniqueness:true validation technique"  do 
    
  @user1=@user.dup
  

  @user.save

  assert_not @user1.valid?
    
    
  end
  
  
  test "uniqueness case sensitivity false checker" do 
    
  
  user1=@user.dup
  @user.save
  user1.email="Sameercourse@gmail.com"

  assert_not user1.valid?
  
    
  end
  
  test "testing if the email is bening saved in downcase or not" do
    
  
  upcaseemail="SAMEERCOURSE@GMAIL.COM"
  @user.email=upcaseemail
  
  @user.save
  
  assert_equal upcaseemail.downcase, @user.reload.email
  
    
    
  end
  
  
  test "password should not be blank" do
    
  
  @user.password = " "*6
  assert_not @user.valid?
  
    
    
  end
  
  test "password should not be less than 5 characters" do
    
  @user.password=@user.password_confirmation="a"*5;
  assert_not @user.valid?
    
  end
  
  
  
 
  
  
  
  
  
  
  
end
